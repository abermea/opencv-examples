#include <iostream>
#include <vector>
#include <algorithm>
#include <opencv2\opencv.hpp>

void main() {
	// Create an object to capture from device
	cv::VideoCapture cap(0);

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return;
	}

	// Infinite Capture Loop
	while (true) {
		cv::Mat frame, inverse;
		std::vector<cv::Mat> channels(3);

		cap >> frame; // Capture input
		cv::split(frame, channels.data());
		std::swap(channels[0], channels[2]);
		cv::merge(channels.data(), 3, inverse);
		cv::imshow("Test", inverse); // Display
		if (cv::waitKey(30) >= 0) break; // Press ESC to exit the program
	}
}