#include <iostream>
#include <vector>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

void main() {
	// Create an object to capture from device
	cv::VideoCapture cap(1);

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return;
	}

	// Infinite Capture Loop
	while (true) {
		cv::Mat frame, eq;
		std::vector<cv::Mat> channels(3);
		cap >> frame; // Capture input

		cv::split(frame, channels.data()); // Split Channels
		// Equalize each channel separetly
		cv::equalizeHist(channels[0], channels[0]);
		cv::equalizeHist(channels[1], channels[1]);
		cv::equalizeHist(channels[2], channels[2]);
		cv::merge(channels.data(), 3, eq); // Merge Channels Back

		cv::imshow("Original", frame); // Display
		cv::imshow("Equalized", eq); // Display
		if (cv::waitKey(30) >= 0) break; // Press ESC to exit the program
	}
}