#include <iostream>
#include <vector>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

cv::Point anchor(-1, -1);
double delta = 0.0;
int depth = -1;
int kernel_size = 1;
int max_kernel_size = 10;
cv::Mat kernel = cv::Mat::ones(5, 5, CV_32F) / float(5 * 5);

void onKernel(int, void*);

void main() {
	// Create an object to capture from device
	cv::VideoCapture cap(1);

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return;
	}

	// Infinite Capture Loop
	while (true) {
		cv::namedWindow("Test", 1);
		cv::createTrackbar("Kernel Size", "Test", &kernel_size, max_kernel_size, onKernel);

		cv::Mat frame, eq, filter;
		std::vector<cv::Mat> channels(3);
		cap >> frame; // Capture input

		cv::split(frame, channels.data()); // Split Channels
		// Equalize each channel separetly
		cv::equalizeHist(channels[0], channels[0]);
		cv::equalizeHist(channels[1], channels[1]);
		cv::equalizeHist(channels[2], channels[2]);
		cv::merge(channels.data(), 3, eq); // Merge Channels Back

		cv::filter2D(eq, filter, depth, kernel, anchor, delta);

		cv::imshow("Test", filter); // Display
		if (cv::waitKey(30) >= 0) break; // Press ESC to exit the program
	}
}

void onKernel(int, void*) {
	int actual_size = kernel_size * 2 + 1;
	kernel = cv::Mat::ones(actual_size, actual_size, CV_32F) / float(actual_size * actual_size);
}