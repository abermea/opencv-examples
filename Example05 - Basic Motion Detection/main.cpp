#include <iostream>
#include <vector>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

cv::Point anchor(-1, -1);
cv::Mat kernel = cv::Mat::ones(1, 1, CV_32F);
cv::Mat first;

void main() {
	// Create an object to capture from device
	cv::VideoCapture cap(1);

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return;
	}

	cap >> first;
	cv::cvtColor(first, first, CV_BGR2GRAY);
	cv::equalizeHist(first, first);

	// Infinite Capture Loop
	while (true) {
		cv::Mat frame, delta, thresh, canny, dst;
		std::vector<std::vector<cv::Point>> contours;

		cap >> frame; // Capture input

		cv::cvtColor(frame, frame, CV_BGR2GRAY);
		cv::blur(frame, frame, cv::Size(5, 5));
		cv::equalizeHist(frame, frame);
		cv::absdiff(first, frame, delta);
		cv::threshold(delta, thresh, 50, 255, CV_THRESH_BINARY);
		cv::Canny(thresh, canny, 10, 40, 3);
		dst = cv::Scalar::all(0);

		frame.copyTo(dst, canny);

		//findContours(delta, contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

		cv::imshow("First", first); // Display
		cv::imshow("Original", frame); // Display
		cv::imshow("Delta", delta); // Display
		cv::imshow("Canny", canny);
		cv::imshow("Masked", dst);


		cv::swap(first, frame);
		if (cv::waitKey(50) >= 0) break; // Press ESC to exit the program
	}
}