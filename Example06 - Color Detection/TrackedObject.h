#pragma once
#include <deque>
#include <vector>
#include <string>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>
class TrackedObject
{
	std::deque<cv::Vec3i> trail;
	std::string name;
	
public:
	TrackedObject();
	~TrackedObject();

	void DrawTrail(cv::Mat& image);
	void DrawCircumference(cv::Mat& image);
	void DrawName(cv::Mat& image);
	void AppendToTrail(cv::Vec3i& point);
	TrackedObject(std::string name);
	TrackedObject(std::string name, cv::Vec3i position);
	cv::Vec3i GetLastKnownPosition();

	friend bool operator==(const TrackedObject& a, const TrackedObject& o);
};

bool operator==(const TrackedObject& a, const TrackedObject& o);
