#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <functional>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include "TrackedObject.h"

cv::Scalar full2opencvRange(int h, int s, int v);
bool cmp(const cv::Vec3i& a, const cv::Vec3i& b);

const int slider_1_max = 100;
const int slider_2_max = 100;
const int denom_max = 15;
int slider_1 = 0;
int slider_2 = 0;
int denom = 0;

void main() {
	// Create an object to capture from device
	cv::VideoCapture cap(0);
	std::string base_name = "circulo ";

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return;
	}

	std::vector<TrackedObject> tracked;

	// Infinite Capture Loop
	while (true) {
		cv::Mat frame, hsv_frame;
		std::vector<std::vector<cv::Point>> contours;
		std::vector<cv::Vec4i> hierarchy;

		cv::namedWindow("circulos");

		cap >> frame; // Capture input
		cv::GaussianBlur(frame, frame, cv::Size(9, 9), 2);
		cv::cvtColor(frame, hsv_frame, CV_BGR2HSV);

		cv::Mat lower_hue_range;
		cv::inRange(hsv_frame, full2opencvRange(0, 50, 40), full2opencvRange(20, 100, 100), lower_hue_range);

		cv::Mat upper_hue_range;
		cv::inRange(hsv_frame, full2opencvRange(340, 50, 40), full2opencvRange(360, 100, 100), upper_hue_range);

		cv::Mat mixed;
		cv::addWeighted(lower_hue_range, 1.0, upper_hue_range, 1.0, 0.0, mixed);
		cv::GaussianBlur(mixed, mixed, cv::Size(9, 9), 2);
		cv::imshow("deteccion", mixed);

		//cv::createTrackbar("Param 5", "circulos", &denom, denom_max);
		//cv::createTrackbar("Param 6", "circulos", &slider_1, slider_1_max);
		//cv::createTrackbar("Param 7", "circulos", &slider_2, slider_2_max);

		std::vector<cv::Vec3f> circles;
		cv::HoughCircles(lower_hue_range, circles, CV_HOUGH_GRADIENT, 1, lower_hue_range.rows / 2, 101, 16);

		int circle_counter = 0;
		if (circles.size() != 0) {
			std::sort(circles.begin(), circles.end(), cmp);

			for (auto& circle : circles)
			{
				auto value = cv::Vec3i(circle[0], circle[1], circle[2]);
				bool add = false;

				std::function<bool(TrackedObject&)> pred = [&](TrackedObject& obj) {
					auto lastPos = obj.GetLastKnownPosition();
					int a = value[0] - lastPos[0];
					int b = value[1] - lastPos[1];
					int distance = a*a + b*b;
					distance = std::sqrt(distance);

					return distance < lastPos[2] + value[2];
				};

				auto found = std::find_if(tracked.begin(), tracked.end(), pred);
				if (found != tracked.end()) {
					found->AppendToTrail(value);
				}
				else {
					std::string name = "circulo " + std::to_string(circle_counter);
					TrackedObject to(name, value);
					tracked.push_back(to);
					circle_counter += 1;
				}
			}

			if (tracked.size() > circles.size()) {
				std::vector<TrackedObject> remove_queue;
				for (auto obj : tracked) {
					auto lastPos = obj.GetLastKnownPosition();
					bool add = false;

					std::function<bool(cv::Vec3f&)> pred = [&](cv::Vec3f& value) {
						int a = value[0] - lastPos[0];
						int b = value[1] - lastPos[1];
						int distance = a*a + b*b;
						distance = std::sqrt(distance);

						return distance > lastPos[2] + value[2];
					};

					auto found = std::find_if(circles.begin(), circles.end(), pred);
					if (found != circles.end()) {
						remove_queue.push_back(obj);
					}
				}

				for (auto obj : remove_queue) {
					tracked.erase(std::remove(tracked.begin(), tracked.end(), obj), tracked.end());
				}
			}
		}
		else {
			tracked.clear();
			circle_counter = 0;
		}

		for each (TrackedObject obj in tracked)
		{
			obj.DrawCircumference(frame);
			obj.DrawName(frame);
			obj.DrawTrail(frame);
		}

		cv::imshow("circulos", frame);

		if (cv::waitKey(50) >= 0)
			break; // Press ESC to exit the program
	}

}

cv::Scalar full2opencvRange(int h, int s, int v) {
	return cv::Scalar(h / 2, (s * 255)*0.01f, (v * 255)*0.01f);
}

bool cmp(const cv::Vec3i& a, const cv::Vec3i& b) {
	return a[2] < b[2];
}
