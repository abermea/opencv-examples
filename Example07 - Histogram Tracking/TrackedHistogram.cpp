#include "TrackedHistogram.h"



TrackedHistogram::TrackedHistogram()
{
}


TrackedHistogram::~TrackedHistogram()
{
}


TrackedHistogram::TrackedHistogram(std::string& name, cv::Mat& histogram) : name(name), histogram(histogram)
{
}


bool TrackedHistogram::CompareHistogram(cv::Mat& histogram, double threshold)
{
	double result = cv::compareHist(this->histogram, histogram, CV_COMP_CORREL);
	return result > threshold;
}


std::string TrackedHistogram::getName()
{
	return name;
}


void TrackedHistogram::DrawName(cv::Mat& image)
{
	cv::putText(image, this->getName(), cv::Point(10, 10), CV_FONT_HERSHEY_PLAIN, 1, cv::Scalar(0, 255, 0));
}
