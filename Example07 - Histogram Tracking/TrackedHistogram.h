#pragma once
#include <cstring>
#include <vector>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

class TrackedHistogram
{
	cv::Mat histogram;
	std::string name;
	TrackedHistogram();
public:
	~TrackedHistogram();
	TrackedHistogram(std::string& name, cv::Mat& histogram);
	bool CompareHistogram(cv::Mat& histogram, double threshold);
	std::string getName();
	void DrawName(cv::Mat& image);
};

