#include <iostream>
#include <functional>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

#include "TrackedHistogram.h"

cv::Scalar full2opencvRange(int h, int s, int v);

int main() {

	// Create an object to capture from device
	cv::VideoCapture cap(1);

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return 1;
	}

	int low_s = 0;
	int low_v = 0;
	int hi_s = 100;
	int hi_v = 45;

	std::vector<TrackedHistogram> tracked;

	while (true) {
		cv::Mat frame;
		cap >> frame;
		/*cv::Rect rect = cv::Rect(frame.size[1] / 2, frame.size[0] / 2, frame.size[1] / 2, frame.size[0] / 2);
		frame = cv::Mat(frame, rect);*/
		cv::GaussianBlur(frame, frame, cv::Size(3, 3), 3);

		cv::Mat hsv_frame;
		cv::cvtColor(frame, hsv_frame, CV_BGR2HSV);

		cv::Mat hue_range;
		cv::inRange(hsv_frame, full2opencvRange(0, 0, 0), full2opencvRange(360, hi_s, hi_v), hue_range);
		cv::GaussianBlur(hue_range, hue_range, cv::Size(5, 5), 2, 2);
		//cv::bitwise_not(hue_range, hue_range);
		cv::imshow("blancos", hue_range);

		cv::createTrackbar("HI S", "blancos", &hi_s, 100);
		cv::createTrackbar("HI V", "blancos", &hi_v, 100);

		cv::Mat masked;
		frame.copyTo(masked, hue_range);
		cv::imshow("mascara", masked);

		if (cv::countNonZero(hue_range) > 100)
		{
			cv::Mat histogram;
			int channels[] = { 0 };
			int histSize[] = { 32 };
			float range[] = { 0, 256 };
			const float* ranges[] = { range };
			cv::calcHist(&frame, 1, channels, hue_range, histogram, 1, histSize, ranges, true, false);

			std::function<bool(TrackedHistogram&)> pred = [&](TrackedHistogram& obj) {
				return obj.CompareHistogram(histogram, 0.8);
			};

			auto res = std::find_if(tracked.begin(), tracked.end(), pred);

			if (res != tracked.end()) {
				res->DrawName(frame);
			} else {
				auto th = TrackedHistogram("Objeto " + std::to_string(tracked.size()), histogram);
				tracked.push_back(th);
				th.DrawName(frame);
			}
		}
		else {
			cv::putText(frame, "Ningun Objeto", cv::Point(10, 10), CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 255));
		}

		cv::imshow("original", frame);
		if (cv::waitKey(150) >= 0)
			break; // Press ESC to exit the program
	}

	return 0;
}

cv::Scalar full2opencvRange(int h, int s, int v) {
	return cv::Scalar(h / 2, (s * 255)*0.01f, (v * 255)*0.01f);
}