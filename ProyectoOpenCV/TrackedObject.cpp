#include "TrackedObject.h"



TrackedObject::TrackedObject()
{
}


TrackedObject::~TrackedObject()
{
}


void TrackedObject::DrawTrail(cv::Mat& image)
{
	for (cv::Vec3i vec : trail) {
		cv::Point center(std::round(vec[0]), std::round(vec[1]));
		cv::circle(image, center, 1, cv::Scalar(255, 0, 0), 2);
	}
}


void TrackedObject::DrawCircumference(cv::Mat& image)
{
	if (!trail.empty()) {
		cv::Vec3i vec = trail.back();
		cv::Point center(std::round(vec[0]), std::round(vec[1]));
		int radius = std::round(vec[2]);
		cv::circle(image, center, radius, cv::Scalar(0, 255, 0), 2);
	}
}


void TrackedObject::DrawName(cv::Mat& image)
{
	if (!trail.empty()) {
		cv::Vec3i vec = trail.back();
		cv::Point center(std::round(vec[0]), std::round(vec[1]));
		cv::putText(image, cv::String(name), center, CV_FONT_HERSHEY_PLAIN, 1.0, cv::Scalar(0, 0, 0));
	}
}


void TrackedObject::AppendToTrail(cv::Vec3i& point)
{
	if (trail.size() >= 100) {
		trail.pop_front();
	}

	trail.push_back(point);
}


TrackedObject::TrackedObject(std::string name) : name(name)
{
}
