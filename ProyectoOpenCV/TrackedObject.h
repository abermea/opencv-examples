#pragma once
#include <deque>
#include <vector>
#include <string>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>
class TrackedObject
{
	std::deque<cv::Vec3i> trail;
	std::string name;
	
	TrackedObject();
public:
	~TrackedObject();

	void DrawTrail(cv::Mat& image);
	void DrawCircumference(cv::Mat& image);
	void DrawName(cv::Mat& image);
	void AppendToTrail(cv::Vec3i& point);
	TrackedObject(std::string name);
};

