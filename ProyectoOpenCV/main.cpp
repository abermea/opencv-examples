#include <iostream>
#include <opencv2\opencv.hpp>

void main() {
	// Create an object to capture from device
	cv::VideoCapture cap(0);

	// Error handling
	if (!cap.isOpened()) {
		std::cerr << "Unable to capture from camera. Are you sure it's plugged in?" << std::endl;
		return;
	}

	// Infinite Capture Loop
	while (true) {
		// Capture input
		cv::Mat frame;
		cap >> frame;

		cv::imshow("Test", frame); // Display

		if (cv::waitKey(30) >= 0) break; // Press ESC to exit the program
	}
}