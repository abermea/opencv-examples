# OpenCV 3 Samples #
## Version 0.1 ##

## How do I get set up? ##

### Requirements ###
* Visual Studio 2015
* OpenCV 3
* A webcam

### Setup ###
* Clone Repo
* You need to place your OpenCV DLLs in the Debug folder, or somewhere in your System PATH
* If you installed OpenCV anywhere other than the default directory (C:\OpenCV), you will need to update the Include Directories on the project properties